# Гайды Интуиции

Гайды по разработке продуктов для дизайнеров от дизайнеров.

## Как начать работать

1. Поставить python v3, если его нет: https://www.python.org/downloads/release/python-375/

2. Установить зависимости:
    ```bash
    pip3 install mkdocs mkdocs-material markdown-include pymdown-extensions pygments
    ```

4. Запустить сайт:
    ```bash
    mkdocs serve
    ```

5. Перейти в браузере по ссылке: http://127.0.0.1:8000.

6. Файлы со страницами лежат в папке `docs`.

7. Навигацию можно изменить в `mkdocs.yml` в разделе `nav`.
